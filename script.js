var STOPPED = 0;
var READY = 1;
var RUNNING = 2;

var keysDown = [false, false];
var activationKeys = [['q', 'w', 'e', 'a', 's', 'd', 'z', 'x', 'c'], ['i', 'o', 'p', 'k', 'l', 'ö', ',', '.', '-', 'control']];
var keyIndicators = [];
var handIndicators = [];
var timerState = STOPPED;
var scrambleElement = null;
var timerElement = null;
var timerUpdater = null;
var startTime = null;
var times = [];

function init() {
	document.body.onkeydown = keyDownHandler;
	document.body.onkeyup = keyUpHandler;
	timerElement = document.getElementById('timer');
	scrambleElement = document.getElementById('scramble');
	handIndicators = [document.getElementById('left'), document.getElementById('right')];
	// createActivationKeyIndicators();
	addTouchListeners();
	newScramble();
}

function keyDownHandler(ev) {
	var allDown = true;
	for (var i = 0; i < activationKeys.length; i++) {
		for (var j = 0; j < activationKeys[i].length; j++) {
			if (ev.key.toLowerCase() === activationKeys[i][j]) {
				keysDown[i] = true;
				// enableKeyIndicator(keyIndicators[i][j]);
				handIndicators[i].style.opacity = 0.9;
			}
		}
		if(!keysDown[i]) {
			allDown = false;
		}
	}
	if(allDown) {
		switch(timerState) {
			case STOPPED:
				readyTimer();
				hideScramble();
				break;
			case RUNNING:
				stopTimer();
				newScramble();
				showScramble();
				break;
		}
	}
}

function keyUpHandler(ev) {
	var allDown = true;
	for(var i = 0; i < activationKeys.length; i++) {
		keysDown[i] = false;
		for (var j = 0; j < activationKeys[i].length; j++) {
			if (ev.key.toLowerCase() === activationKeys[i][j]) {
				keysDown[i] = false;
				// disableKeyIndicator(keyIndicators[i][j]);
				handIndicators[i].style.opacity = 0.5;
			}
		}
		if(!keysDown[i] && timerState === READY) {
			startTimer();
		};
	}
}

function generateScramble(length) {
	// L, R, F, B, U, D
	var sidesAvailable = [true, true, true, true, true, true];
	var oppositeSides = [1, 0, 3, 2, 5, 4];
	var scramble = [];
	for(var i = 0; i < length; i++) {
		do {
			var side = Math.floor(Math.random()*6);
		} while(!sidesAvailable[side])
		var turn = Math.floor(Math.random()*3);
		scramble.push({'side' : side, 'turn' : turn})
		for(var j = 0; j < 6; j++) {
			if(j !== oppositeSides[side]) {
				sidesAvailable[j] = true;
			}
		}
		sidesAvailable[side] = false;
	}
	return scramble;
}

function scrambleToHtml(scramble) {
	var sideNames = ['L','R','F','B','U','D'];
	var output = '';
	for(var i = 0; i < scramble.length; i++) {
		output += sideNames[scramble[i].side];
		if(scramble[i].turn === 1) {
			output += '2';
		} else if(scramble[i].turn === 2) {
			output += 'i';
		}
		if(i+1 < scramble.length) {
			output += ' ';
		}
	}
	return output;
}

function readyTimer() {
	timerState = READY;
	timerElement.innerHTML = '0:00.000';
}

function startTimer() {
	timerState = RUNNING;
	startTime = new Date().getTime();
	timerUpdater = requestAnimationFrame(updateTimer);
}

function stopTimer() {
	timerState = STOPPED;
	var endTime = new Date().getTime();
	if(timerUpdater) cancelAnimationFrame(timerUpdater);
	var elapsedTime = (endTime - startTime) / 1000;
	setCurrentTime(elapsedTime);
	logTime(elapsedTime);
	updateAverages();
}

function updateTimer() {
	var currentTime = new Date().getTime();
	var elapsedTime = (currentTime - startTime) / 1000;
	setCurrentTime(elapsedTime);
	if(timerState === RUNNING) {
		timerUpdater = requestAnimationFrame(updateTimer);
	} else {
		cancelAnimationFrame(timerUpdater);
		timerUpdater = null;
	}
}

function setCurrentTime(time) {
	timerElement.innerHTML = formatTime(time);
}

function formatTime(time) {
	var minutes = Math.floor(time / 60);
	var seconds = Math.floor(time % 60);
	var milliseconds = Math.round((time % 60 - seconds) * 1000);
	timeString = minutes + ':' + (seconds < 10 ? '0' : '') + seconds + '.' + (milliseconds < 100 ? '0' : '') + (milliseconds < 10 ? '0' : '') + milliseconds;
	return timeString;
}

function logTime(time) {
	times.push(time);
}

function updateAverages() {
	if(times.length >= 5) {
		var last5 = times.slice(times.length - 5);
		var highest = getHighest(last5);
		var lowest = getLowest(last5);
		last5.splice(highest, 1);
		last5.splice(lowest, 1);
		var ao5 = getMean(last5);
		document.getElementById('ao5').innerText = formatTime(ao5);
	}
	
	if(times.length >= 12) {
		var last12 = times.slice(times.length - 12);
		var highest = getHighest(last12);
		var lowest = getLowest(last12);
		last12.splice(highest, 1);
		last12.splice(lowest, 1);
		var ao12 = getMean(last12);
		document.getElementById('ao12').innerText = formatTime(ao12);
	}
}

function getHighest(times) {
	var highest = 0;
	for(var i = 1, l = times.length; i < l; i++) {
		if(times[i] > times[highest]) {
			highest = i;
		}
	}
	return highest;
}

function getLowest(times) {
	var lowest = 0;
	for(var i = 1, l = times.length; i < l; i++) {
		if(times[i] < times[lowest]) {
			lowest = i;
		}
	}
	return lowest;
}

function getMean(times) {
	var sum = 0;
	for(var i = 0, l = times.length; i < l; i++) {
		sum += times[i];
	}
	return sum / times.length;
}

function newScramble() {
	scrambleElement.innerHTML = scrambleToHtml(generateScramble(15));
}

function clearScramble() {
	scrambleElement.innerHTML = '';
}

function hideScramble() {
	scrambleElement.style.opacity = 0;
}

function showScramble() {
	scrambleElement.style.opacity = 1;
}

function createActivationKeyIndicators() {
	var keyIndicatorTemplate = document.createElement('div');
	keyIndicatorTemplate.classList.add('key-indicator');
	keyIndicatorTemplate.appendChild(document.createTextNode(''));
	
	for(var i = 0, l = activationKeys.length; i < l; i++) {
		keyIndicators[i] = [];
		for(var j = 0, m = activationKeys[i].length; j < m; j++) {
			var keyIndicator = keyIndicatorTemplate.cloneNode(true);
			var keyTitle = activationKeys[i][j];
			keyTitle = keyTitle.charAt(0).toUpperCase() + keyTitle.slice(1);
			keyIndicator.firstChild.nodeValue = keyTitle;
			handIndicators[i].appendChild(keyIndicator);
			keyIndicators[i][j] = keyIndicator;
		}
	}
}

function enableKeyIndicator(keyIndicator) {
	keyIndicator.style.opacity = 1;
}

function disableKeyIndicator(keyIndicator) {
	keyIndicator.style.opacity = 0.5;
}

function addTouchListeners() {
	for(var i = 0, l = handIndicators.length; i < l; i++) {
		handIndicators[i].addEventListener('touchstart', touchStartHandler, true);
		handIndicators[i].addEventListener('touchend', touchEndHandler, true);
	}
}

function touchStartHandler(ev) {
	for(var i = 0, l = handIndicators.length; i < l; i++) {
		if(handIndicators[i] == ev.target) {
			keysDown[i] = true;
		}
	}
	checkHands();
}

function touchEndHandler(ev) {
	for(var i = 0, l = handIndicators.length; i < l; i++) {
		if(handIndicators[i] == ev.target) {
			keysDown[i] = false;
		}
	}
	checkHands();
}

function checkHands() {
	var allDown = true;
	for(var i = 0; i < activationKeys.length; i++) {
		if(keysDown[i]) {
			handIndicators[i].style.opacity = 1;
		} else {
			handIndicators[i].style.opacity = 0.5;
			allDown = false;
		}
	}
	
	if(allDown) {
		switch(timerState) {
			case STOPPED:
				readyTimer();
				hideScramble();
				break;
			case RUNNING:
				stopTimer();
				newScramble();
				showScramble();
				break;
		}
	} else {
		switch(timerState) {
			case READY:
				startTimer();
		}
	}
}